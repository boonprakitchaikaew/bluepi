#!/usr/bin/env node

const amqp = require('amqplib/callback_api')
const fetch = require('node-fetch')

const url = `amqp://${process.env.RABBITMQ_DEFAULT_USER}:${process.env.RABBITMQ_DEFAULT_PASS}@${process.env.RABBITMQ_DEFAULT_HOST}:${process.env.RABBITMQ_DEFAULT_PORT}/`

const app = () => {
    amqp.connect(url, function(error0, connection) {
        if (error0) {
            console.info('connecting to rabbitmq failed, waiting to retry')
            setTimeout(() => {
                app()
            }, 1000)
            return
        }
    
        connection.createChannel(function(error1, channel) {
            if (error1) {
                throw error1
            }
    
            var queue = 'new-highest-score'
    
            channel.assertQueue(queue, {
                durable: false
            })
    
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue)
    
            channel.consume(queue, function(msg) {
                console.log(" [x] Received %s", msg.content.toString())
    
                // call boardcast api in notification service 
                fetch('http://backend-notification:7000/boardcast', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ user_id: msg.content.toString() })
                })
            }, {
                noAck: true
            })
        })
    })
}

app()