
import os
import random
import string

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

class DevelopmentConfig(object):
    APP_FOLDER = "app/"
    
     # Flask-Via
    VIA_ROUTES_MODULE = "app.routes"

    #Flask-SQLAlchemy
    SQLALCHEMY_DATABASE_URI = "postgresql://{0}:{1}@db:5432/{2}".format(os.getenv("POSTGRES_USER"), os.getenv("POSTGRES_PASSWORD") , os.getenv("POSTGRES_DB"))
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    POSSIBLE_CARD_NUMBERS = [1, 1, 2, 2 , 3, 3, 4, 4 ,5 ,5, 6, 6]

    # JWT
    JWT_SECRET="secret"

    RABBITMQ_DEFAULT_USER = os.getenv("RABBITMQ_DEFAULT_USER")
    RABBITMQ_DEFAULT_PASS = os.getenv("RABBITMQ_DEFAULT_PASS")
    RABBITMQ_DEFAULT_VHOST = os.getenv("RABBITMQ_DEFAULT_VHOST")
    RABBITMQ_DEFAULT_HOST = os.getenv("RABBITMQ_DEFAULT_HOST")
    RABBITMQ_DEFAULT_PORT = os.getenv("RABBITMQ_DEFAULT_PORT")

class TestingConfig(DevelopmentConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///{}".format(os.path.join(BASE_DIR, "testing.db"))

    POSSIBLE_CARD_NUMBERS = [1, 1, 2, 2]

app_config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig
}
