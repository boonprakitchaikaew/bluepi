import unittest

from app import create_app
from app.user.models import User
from app.models import *
from flask import json
from flask_testing import TestCase

from utils.jwt import generate_jwt

class TestGame(TestCase):
    def create_app(self):
        config_name = "testing"
        self.app = create_app(config_name)
        return self.app

    def setUp(self):
        db.session.commit()
        db.drop_all()
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_should_return_200_with_token_and_user_id(self):
        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user = json.loads(response.data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(user["token"], generate_jwt(user["user_id"]).decode("utf-8"))

        users = User.query.filter_by(id=user["user_id"]).all()
        self.assertEqual(len(users), 1)

if __name__ == "__main__":
    unittest.main()
