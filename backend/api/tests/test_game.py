import unittest
import uuid

from app import create_app
from app.game.models import Card, Game
from app.models import *
from flask import json
from flask_testing import TestCase
from mock import patch

class TestGame(TestCase):
    def create_app(self):
        config_name = "testing"
        self.app = create_app(config_name)
        return self.app

    def setUp(self):
        db.session.commit()
        db.drop_all()
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_should_return_401_when_no_authorization(self):
        response = self.app.test_client().post(
            "/api/games/", 
            data=json.dumps({}), 
            headers={"Authorization": ""}, 
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 401)

    def test_should_be_able_to_create_a_new_game(self):
        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user = json.loads(response.data)

        response = self.app.test_client().post(
            "/api/games/", 
            data=json.dumps({}), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.data)
        
        game = Game.query.filter_by(id=response_data["id"]).first()
        cards = Card.query.filter_by(game_id=game.id).all()

        self.assertTrue(game is not None)
        self.assertEqual(game.user_id, user["user_id"])
        self.assertEqual(game.total_click, 0)
        self.assertEqual(len(cards), 4)

    def test_should_400_when_request_body_is_invalid(self):
        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user = json.loads(response.data)

        response = self.app.test_client().post(
            "/api/games/", 
            data=json.dumps({}), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        response_data = json.loads(response.data)
        
        game = Game.query.filter_by(id=response_data["id"]).first()
        response = self.app.test_client().post(
            "/api/games/{0}".format(game.id), 
            data=json.dumps({ }), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 400)

    def test_should_changed_card_status_to_none_when_cards_are_not_matched(self):
        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user = json.loads(response.data)

        response = self.app.test_client().post(
            "/api/games/", 
            data=json.dumps({}), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.data)
        
        game = Game.query.filter_by(id=response_data["id"]).first()
        cards = Card.query.filter_by(game_id=game.id, number=1).all()
        card_1 = cards[0]
        card_2 = cards[1]
        cards = Card.query.filter_by(game_id=game.id, number=2).all()
        card_3 = cards[0]
        card_4 = cards[1]
  
        response = self.app.test_client().post(
            "/api/games/{0}".format(game.id), 
            data=json.dumps({ "card_id": card_1.id }), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        updated_card_1 = Card.query.filter_by(game_id=game.id, number=1).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_card_1.status, Card.WAITING_FOR_MATCH)
        updated_game = Game.query.filter_by(id=response_data["id"]).first()
        self.assertEqual(updated_game.total_click, 1)

        response = self.app.test_client().post(
            "/api/games/{0}".format(game.id), 
            data=json.dumps({ "card_id": card_3.id }), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        updated_card_1 = Card.query.filter_by(game_id=game.id, id=card_1.id).first()
        updated_card_3 = Card.query.filter_by(game_id=game.id, id=card_3.id).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_card_1.status, Card.NONE)
        self.assertEqual(updated_card_3.status, Card.NONE)
        updated_game = Game.query.filter_by(id=response_data["id"]).first()
        self.assertEqual(updated_game.total_click, 2)
    
    @patch("app.game.views.channel")
    def test_should_chagned_status_to_matched_when_select_card_match_with_previous_selected_card(self, channel_mock):
        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user = json.loads(response.data)

        response = self.app.test_client().post(
            "/api/games/", 
            data=json.dumps({}), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.data)
        
        game = Game.query.filter_by(id=response_data["id"]).first()
        cards = Card.query.filter_by(game_id=game.id, number=1).all()
        card_1 = cards[0]
        card_2 = cards[1]
        cards = Card.query.filter_by(game_id=game.id, number=2).all()
        card_3 = cards[0]
        card_4 = cards[1]
  
        response = self.app.test_client().post(
            "/api/games/{0}".format(game.id), 
            data=json.dumps({ "card_id": card_1.id }), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        updated_card_1 = Card.query.filter_by(game_id=game.id, id=card_1.id).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_card_1.status, Card.WAITING_FOR_MATCH)
        updated_game = Game.query.filter_by(id=response_data["id"]).first()
        self.assertEqual(updated_game.total_click, 1)

        response = self.app.test_client().post(
            "/api/games/{0}".format(game.id), 
            data=json.dumps({ "card_id": card_2.id }), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        updated_card_1 = Card.query.filter_by(game_id=game.id, id=card_1.id).first()
        updated_card_2 = Card.query.filter_by(game_id=game.id, id=card_2.id).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_card_1.status, Card.MATCHED)
        self.assertEqual(updated_card_2.status, Card.MATCHED)
        updated_game = Game.query.filter_by(id=response_data["id"]).first()
        self.assertEqual(updated_game.total_click, 2)

        response = self.app.test_client().post(
            "/api/games/{0}".format(game.id), 
            data=json.dumps({ "card_id": card_3.id }), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        updated_card_3 = Card.query.filter_by(game_id=game.id, id=card_3.id).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_card_3.status, Card.WAITING_FOR_MATCH)
        updated_game = Game.query.filter_by(id=response_data["id"]).first()
        self.assertEqual(updated_game.total_click, 3)

        response = self.app.test_client().post(
            "/api/games/{0}".format(game.id), 
            data=json.dumps({ "card_id": card_4.id }), 
            headers={"Authorization": str(user["token"])}, 
            content_type="application/json"
        )
        updated_card_3 = Card.query.filter_by(game_id=game.id, id=card_1.id).first()
        updated_card_4 = Card.query.filter_by(game_id=game.id, id=card_2.id).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_card_3.status, Card.MATCHED)
        self.assertEqual(updated_card_4.status, Card.MATCHED)
        updated_game = Game.query.filter_by(id=response_data["id"]).first()
        self.assertEqual(updated_game.total_click, 4)
        self.assertEqual(updated_game.status, Game.COMPLETED)

    def test_should_return_403_when_edit_game_that_is_not_owned(self):
        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user1 = json.loads(response.data)

        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user2 = json.loads(response.data)
        
        response = self.app.test_client().post(
            "/api/games/", 
            data=json.dumps({}), 
            headers={"Authorization": str(user1["token"])}, 
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.data)
        
        game = Game.query.filter_by(id=response_data["id"]).first()
        card_1 = Card.query.filter_by(game_id=game.id).first()
  
        response = self.app.test_client().post(
            "/api/games/{0}".format(game.id), 
            data=json.dumps({ "card_id": card_1.id }), 
            headers={"Authorization": str(user2["token"])}, 
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 403)


class TestGameScore(TestCase):
    def create_app(self):
        config_name = "testing"
        self.app = create_app(config_name)
        return self.app

    def setUp(self):
        db.session.commit()
        db.drop_all()
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_should_return_401_when_no_authorization(self):
        response = self.app.test_client().get(
            "/api/games/scores", 
            data=json.dumps({}), 
            headers={"Authorization": ""}, 
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 401)

    def test_shoule_still_return_200_when_there_is_no_game_in_db(self):
        num_rows_deleted = db.session.query(Game).delete()
        db.session.commit()

        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user = json.loads(response.data)

        response = self.app.test_client().get(
            "/api/games/scores", 
            headers={"Authorization": str(user["token"])}
        )
        
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.data)
        
        self.assertEqual(response_data["grobal_best_score"], None)
        self.assertEqual(response_data["my_best_score"], None)

    def test_shoule_still_return_200__with_completed_best_scores(self):
        response = self.app.test_client().post(
            "/api/users/", 
            data=json.dumps({}), 
            content_type="application/json"
        )
        user = json.loads(response.data)

        # mock data
        my_game_1 = Game(id = str(uuid.uuid4()), user_id = user["user_id"], total_click = 100, status=Game.COMPLETED)
        db.session.add(my_game_1)

        my_game_2 = Game(id = str(uuid.uuid4()), user_id = user["user_id"], total_click = 2, status=Game.IN_PROGRESS)
        db.session.add(my_game_2)

        my_game_3 = Game(id = str(uuid.uuid4()), user_id = user["user_id"], total_click = 12, status=Game.COMPLETED)
        db.session.add(my_game_3)

        grobal_game_1 = Game(id = str(uuid.uuid4()), user_id = str(uuid.uuid4()), total_click = 65, status=Game.COMPLETED)
        db.session.add(grobal_game_1)

        grobal_game_2 = Game(id = str(uuid.uuid4()), user_id = str(uuid.uuid4()), total_click = 1, status=Game.IN_PROGRESS)
        db.session.add(grobal_game_2)

        grobal_game_3 = Game(id = str(uuid.uuid4()), user_id = str(uuid.uuid4()), total_click = 4, status=Game.COMPLETED)
        db.session.add(grobal_game_3)

        db.session.commit()
        
        response = self.app.test_client().get(
            "/api/games/scores", 
            headers={"Authorization": str(user["token"])}
        )
        self.assertEqual(response.status_code, 200)
        response_data = json.loads(response.data)
        
        self.assertEqual(response_data["grobal_best_score"], 4)
        self.assertEqual(response_data["my_best_score"], 12)

if __name__ == "__main__":
    unittest.main()
