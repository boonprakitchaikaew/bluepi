import os

import jwt
import psycopg2
from config import app_config
from flask import Blueprint, current_app


def generate_jwt(sub):
    return jwt.encode({"sub": sub}, current_app.config["JWT_SECRET"], algorithm="HS256")

def validate_jwt(token):
    try:
        return jwt.decode(token, current_app.config["JWT_SECRET"], algorithms=["HS256"], verify=True)
    except Exception as e:
        return 401
