from app.models import db
from sqlalchemy import (JSON, Boolean, Column, DateTime, ForeignKey, Integer,
                        String)
from sqlalchemy.sql import func

# TODO add foreigner key 

class Game(db.Model):
    __tablename__ = "game"
    id = db.Column(String, primary_key=True, unique=True, nullable=False)
    user_id = db.Column(String, nullable=False)
    total_click = Column(Integer, nullable=False)
    status = Column(String, nullable=False, default="IN_PROGRESS") 
    created = Column(DateTime, server_default=func.now())
    updated = Column(DateTime, server_default=func.now())

    IN_PROGRESS = 'IN_PROGRESS'
    COMPLETED = 'COMPLETED'
    
    
class Card(db.Model):
    __tablename__ = "card"
    id = db.Column(String, primary_key=True, unique=True, nullable=False)
    game_id = db.Column(String, nullable=False)
    number = Column(Integer, nullable=False)
    status = Column(String, nullable=False, default="NONE") 
    created = Column(DateTime, server_default=func.now())

    WAITING_FOR_MATCH = 'WAITING_FOR_MATCH'
    MATCHED = 'MATCHED'
    NONE = 'NONE'
