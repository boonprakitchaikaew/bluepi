import datetime
import uuid

from cerberus import Validator
from config import app_config
from flask import Blueprint, current_app, jsonify, request
from flask.views import MethodView
from sqlalchemy import desc
from utils.jwt import validate_jwt

from ..models import db
from ..worker import channel
from .models import Card, Game
from .validator import open_game_card_schema

game_router = Blueprint("game_router", __name__)

@game_router.route("/", methods=["POST"])
def game():
    # TODO removed hardcode validate jwt
    token_result = validate_jwt(request.headers.get("Authorization"))
    if token_result == 401:
        return "Unauthorized", 401

    game = Game(
        id = str(uuid.uuid4()),
        user_id = token_result["sub"],
        total_click = 0
    )
    db.session.add(game)

    cards = []
    for number in current_app.config["POSSIBLE_CARD_NUMBERS"]:
        card_id = str(uuid.uuid4())
        card = Card(
            id = card_id,
            game_id = game.id,
            number = number,
            status = Card.NONE
        )
        cards.append(card_id)
        db.session.add(card)

    db.session.commit()

    return jsonify({
        "id": game.id,
        "total_click": game.total_click,
        "user_id": game.user_id,
        "cards": cards,
        "created": game.created,
    })


@game_router.route("/scores", methods=["GET"])
def game_scores():
    token_result = validate_jwt(request.headers.get("Authorization"))
    if token_result == 401:
        return "Unauthorized", 401

    grobal_best_game = Game.query.filter_by(status=Game.COMPLETED).order_by(Game.total_click).first()
    my_best_game = Game.query.filter_by(status=Game.COMPLETED, user_id=token_result["sub"]).order_by(Game.total_click).first()

    return jsonify({
        "grobal_best_score": grobal_best_game.total_click if grobal_best_game is not None else None,
        "my_best_score": my_best_game.total_click if my_best_game is not None else None
    })


@game_router.route("/<game_id>", methods=["POST"])
def open_game_card(game_id):
    token_result = validate_jwt(request.headers.get("Authorization"))
    if token_result == 401:
        return "Unauthorized", 401

    game = Game.query.filter_by(id=game_id).first()
    best_score_game = Game.query.filter_by(status=Game.COMPLETED).order_by(Game.total_click).first()

    if token_result["sub"] != game.user_id:
        return "Forbidden", 403

    request_body = request.json

    # Validate request body
    validator = Validator(open_game_card_schema)
    if not validator.validate(request_body):
        return "Invalid request body", 400

    card_id = request_body["card_id"]

    waiting_for_match_cards = Card.query.filter_by(game_id=game_id, status=Card.WAITING_FOR_MATCH).all()
    compared_card = Card.query.filter_by(game_id=game_id, id=card_id).first()

    if not compared_card:
        return "Not found", 200

    if len(waiting_for_match_cards) == 0:
        compared_card.status = Card.WAITING_FOR_MATCH
    else:
        previous_card = waiting_for_match_cards[0]
        if previous_card.number == compared_card.number and previous_card.id != compared_card.id:
            compared_card.status = Card.MATCHED
            previous_card.status = Card.MATCHED
        else:
            compared_card.status = Card.NONE
            previous_card.status = Card.NONE

    completed_cards = Card.query.filter_by(game_id=game_id, status=Card.MATCHED).all()
    if len(current_app.config["POSSIBLE_CARD_NUMBERS"]) == len(completed_cards):
        game.status = Game.COMPLETED

        try:
            if best_score_game is not None:
                if game.total_click < best_score_game.total_click:
                    channel.publish(token_result["sub"])
            else:
                channel.publish(token_result["sub"])
        except:
            print("socket io for rabbitmq have a problem")

    game.updated = datetime.datetime.now()
    # increase one more clicked
    game.total_click = game.total_click + 1
    db.session.commit()

    return jsonify({ "total_clicked": game.total_click, "number": compared_card.number })
