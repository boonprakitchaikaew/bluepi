import os
import os.path
import time

import pika
from config import app_config
from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy_utils import create_database, database_exists

from .game.views import game_router
from .models import db
from .user.views import user_router
from .worker import channel

def checking_is_db_ready(engine):
    try:
        return database_exists(engine.url)
    except Exception as e:
        print("connect to db failed, waiting for retry")
        print(e)
        time.sleep(1)
        checking_is_db_ready(engine)

def create_app(mode):
    app = Flask(__name__)
    
    custom_config = app_config[mode]
    app.config.from_object(custom_config)
    app.config["JSONIFY_PRETTYPRINT_REGULAR"] = False

    engine = create_engine(app.config["SQLALCHEMY_DATABASE_URI"])
    checking_is_db_ready(engine)

    if not database_exists(engine.url):
        create_database(engine.url)

    app.register_blueprint(user_router, url_prefix="/api/users")
    app.register_blueprint(game_router, url_prefix="/api/games")

    with app.app_context():
        db.init_app(app)
        db.create_all()

    return app
