import os
import time

import pika
from flask import current_app


class Publisher:
    def __init__(self):
        credentials = pika.PlainCredentials(os.getenv("RABBITMQ_DEFAULT_USER"), os.getenv("RABBITMQ_DEFAULT_PASS"))
        parameters = pika.ConnectionParameters("rabbitmq", int(os.getenv("RABBITMQ_DEFAULT_PORT")), os.getenv("RABBITMQ_DEFAULT_VHOST"), credentials)
        self._params = parameters
        self._conn = None
        self._channel = None

    def _connect(self):
        if not self._conn or self._conn.is_closed:
            self._conn = pika.BlockingConnection(self._params)
            self._channel = self._conn.channel()

    def connect(self):
        try:
            self._connect()
        except Exception as e:
            print("connect to rabbitmq failed, waiting for retry")
            # print(e)
            time.sleep(1)
            self.connect()

    def _publish(self, msg):
        self._channel.basic_publish(exchange="", routing_key="new-highest-score", body=msg)
        logging.debug('message sent: %s', msg)

    def publish(self, msg):
        """Publish msg, reconnecting if necessary."""
        try:
            self._publish(msg)
        except pika.exceptions.ConnectionClosed:
            logging.debug('reconnecting to queue')
            self.connect()
            self._publish(msg)

    def close(self):
        if self._conn and self._conn.is_open:
            logging.debug('closing queue connection')
            self._conn.close()

def connectRabbitMQ():
    publisher = Publisher()
    publisher.connect()
    return publisher

channel = None if os.environ.get("TESTING", "false") == "true" else connectRabbitMQ()
