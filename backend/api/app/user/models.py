import uuid

from app.models import db
from sqlalchemy import Column, String


class User(db.Model):
    __tablename__ = "user"
    id = db.Column(String, primary_key=True, unique=True, nullable=False)
