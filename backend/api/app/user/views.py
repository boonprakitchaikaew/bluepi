import uuid

from flask import Blueprint, jsonify
from flask.views import MethodView
from utils.jwt import generate_jwt

from ..models import db
from .models import User

user_router = Blueprint("user_router", __name__)

@user_router.route("/", methods=["POST"])
def register():
    user = User(id = str(uuid.uuid4()))
    db.session.add(user)
    db.session.commit()
    return jsonify({"token": generate_jwt(user.id).decode("utf-8"), "user_id": user.id})
