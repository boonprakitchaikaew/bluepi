# API description

## Register
api for getting the token that need to use for calling the api that require authorization
```
[POST] /api/users/
```
#### Response

- token: [string] jwt that using for calling the api that require authorization
- user_id: [string] id of user


```
Example
{ 
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhbW91bnQiOnsidmFsdWUiOiIxMC4wMDAwIiwiZWRpdGFibGUiOnRydWV9LCJkZXNjcmlwdGlvbiI6ImJ1eSBldmVyaG90aHV0IiwicGF5ZWUiOiJHRERaR1NFU1o3Q1M1V1o0VTdKQklONzY2UUk2NVFLWUw1UzM3UEtQVURQRVQ1Q0xZSUVCNVlUSiIsInVzZXJfdG9rZW4iOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKbGVIQWlPakUxT0RBMk5URXpNeklzSW1semN5STZJbWgwZEhCek9pOHZZWFYwYUM1b2IzUXRibTkzTG1OdmJTOWhjSEF2YVc1MmFXTjBkWE1pTENKemRXSWlPaUl4TlRjeU9EYzFNek14TFhsV1UzUk1YM1l4YkZwbFFYcFBSVWx6WTFsemJYcHBhWGhJYW0xblZGRm9JbjAuV1Ryb1VHdjEtRXBDT2lMX1NOUGpVSlNFSnJabGd4cm1xdUcwUnZfQUJSOCIsImlhdCI6MTU3Mjg3NTM0OSwiZXhwIjoxNTcyODc4OTQ5fQ.aZjaudRaSn-097E4upLd3CWuE5fJCRACpH948Hcuf50",
    "user_id": "b66b0090-cc26-11ea-87d0-0242ac130003"
}

```

## Create New Game
using for create a new game session specific for user
```
[POST] /api/games/
```
Authorazation: Bearer <user_token>

#### Response

- game_id: [string] game session id
- total_click: [number] number of total click, for first game session should always be 0
- user_id: [string] id of user
- cards: [string[]] list of card id, for this game will have 12 cards, the reason to return this because dont want to expose the number of cards to the frontend
- created: [string] timestamp that start game

```
Example

{ 
    "game_id": "22c41880-cc27-11ea-87d0-0242ac130003",
    "total_click": 0,
    "user_id": "b66b0090-cc26-11ea-87d0-0242ac130003",
    "cards": [
        "d371b8e6-cc26-11ea-87d0-0242ac130003", 
        "ff40ad1a-cc26-11ea-87d0-0242ac130003",
        "e22b35f6-cc26-11ea-87d0-0242ac130003",
        "d77cbd78-cc26-11ea-87d0-0242ac130003",
        "e6b326b0-cc26-11ea-87d0-0242ac130003",
        "023edf0a-cc27-11ea-87d0-0242ac130003",
        "fbe5b64c-cc26-11ea-87d0-0242ac130003",
        "e9db9296-cc26-11ea-87d0-0242ac130003",
        "f8d94f40-cc26-11ea-87d0-0242ac130003",
        "f592d05e-cc26-11ea-87d0-0242ac130003",
        "f158db32-cc26-11ea-87d0-0242ac130003",
        "ede7a320-cc26-11ea-87d0-0242ac130003"
    ],
    "created": "Wed Jul 22 2020 21:24:48 GMT+0700 ",
}

```

## Get Scores
get scores for my account and grobal 
```
[GET] /api/games/scores/
```
Authorazation: Bearer <user_token>

#### Response

- grobal_best_score: [number] the best score for all users
- my_best_score: [number]  the best score for specify user base on token that attach with header request

```
Example

{ 
    "grobal_best_score": 12,
    "totamy_best_scorel_click": 24
}

```

## Open Card
when user clicked on any card, need to call this api to increase the total clicked on backend and also checking matched with selected card

```
[POST] /api/games/:game_id/
```
#### Body
```
{
    "card_id": "d371b8e6-cc26-11ea-87d0-0242ac130003"
}
```
- card_id: [string] card id that can get from the response of Create Game api

Authorazation: Bearer <user_token>

#### Response

- total_clicked: [number] total clicked of this game session
- number: [number] number of card, this will use to show to the use

```
Example

{ 
    "total_clicked": 12,
    "number": 2
}

```