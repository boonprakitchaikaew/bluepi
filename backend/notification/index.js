var app = require('express')();
var http = require('http').createServer(app)
var io = require('socket.io')(http)

io.set('origins', '*:*')

const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (req, res) => {
    res.json({})
})

app.post('/boardcast', (req, res) => {
    io.emit('boardcast', {
        user_id: req.body.user_id,
    })

    res.json({
        user_id: req.body.user_id,
    })
})

io.on('connection', (socket) => {
    console.log('a user connected')
})

http.listen(7000, () => {
    console.log('listening on *:7000')
})