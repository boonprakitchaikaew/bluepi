## BluePi Test
This project is created for test with BluePi company and noted that this is just a simple work flow.

### Tech Stacks
- Nginx
- Docker
- Python Flask
- Node + Socket IO
- RabbitMQ
- Postgres
- NextJS

### Required tools
- Docker
- Docker compose
- Minikube (required for testing deployment script on local server)
- Kubectl

### Commands
#### First running on machine
```
> make reset
```
#### Start all services
```
> make up
```
then you can access `localhost:5000` on your browser or with ip on your mobile phone

NOTE: `please wait around 10 seconds` until all services start because rabbitmq service take time  to start

#### Run unittest
```
> make tests
```
#### Restart all services
```
> make reset
```
----
## Backend Document 
[link](/backend/README.md)

----

## Workflow
```mermaid
sequenceDiagram
    participant u as User
    participant f as Frontend service
    participant b as API service
    participant n as Notification service
    u->>f: open game
    alt no token in browser
        f->>b: Register as new user
        note over f,b: [POST] /api/users/
        b-->>f: 200 { token, user_id }
    end
    f-->>f: store in localstorage
    f->>n: subscribe notification
    n-->>f: 200
    f->>b: Create a new game
    note over f,b: [POST] /api/games/
    b-->>f: 200 { id, cards, created, total_clicked }
    f-->>u: show game
    loop
    u->>f: select card
    f->>b: Open card
    note over f,b: [POST]/api/games/:game_id
    alt all cards matched
        b->>n: Create a job to broadcast this point
        n-->>b: 200
    end
    b-->>f: { number, total_clicked }
    f-->>u: show card with number and update total clicked
    end
```

#### NOTED
- User can not see the card number for frontend for sure, until they click the card and get the resposne from the api
- User can not call to the game api that is not belong to yourself

----

## Deployment
1. Edit code in any service
2. Build docker images
```
> make build
```
3. Push docker images to registry (please contact Admin for get credential)
```
> make push 
```
4. Edit Deployment script (Adding service, env, etc) [link](/deployment)
4. Deploy
```
> make deploy
```

#### Try access app on local with Minikube
NOTE: if you want to try deployment on local need to config like this
```
> sudo vi /etc/hosts
```
then add this line (maybe ip will change, please run `kubectl get ingress`)
```
172.17.0.3 bluepi.info
```

then you can access `bluepi.info`