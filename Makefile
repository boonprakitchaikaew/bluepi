TAG = $$(git describe)

build:
	docker-compose build 

push:
	docker-compose push

reset:
	docker network create bluepi-network || echo ""
	docker-compose down
	docker-compose build 

tests:
	docker-compose run backend-api python -m unittest tests/test_user.py
	docker-compose run backend-api python -m unittest tests/test_game.py

up:
	docker-compose down
	docker-compose up -d

deploy: 
	kubectl apply -f deployment/db.yaml
	kubectl apply -f deployment/rabbitmq.yaml
	kubectl apply -f deployment/backend-api.yaml
	kubectl apply -f deployment/backend-notification.yaml
	kubectl apply -f deployment/web.yaml
	kubectl apply -f deployment/worker.yaml
	kubectl apply -f deployment/ingress.yaml