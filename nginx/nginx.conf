server {
    listen 80;
    server_name localhost;

    # this is the internal Docker DNS, cache only for 30s
    resolver 127.0.0.11 valid=30s;
    
    location /api {
        proxy_set_header Host $host;
        proxy_set_header Referer $http_referer;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://backend-api:8000;

        add_header "Access-Control-Allow-Origin"  *;
        add_header "Access-Control-Allow-Methods" "GET, POST, PUT, OPTIONS, HEAD";
        add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';

        # Preflighted requests
        if ($request_method = OPTIONS ) {
            return 200;
        }
    }

    location / {
        proxy_set_header Host $host;
        proxy_set_header Referer $http_referer;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_pass http://web:3000;
    }

    location /socket.io/ {
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://backend-notification:7000/socket.io/;
    }
}
