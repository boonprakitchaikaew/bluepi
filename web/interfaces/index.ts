export interface RegisterResponse {
    token: string
    user_id: string
}

export interface GetNewGameResponse {
    cards: string[],
    id: string
}

export interface GetScoresResponse {
    grobal_best_score: string,
    my_best_score: string
}

export interface OpenCardResponse {
    number: number,
    total_clicked: number
}

export interface Card {
	id: string,
	number: number | null,
	isOpened: boolean,
	isMatched: boolean
}

export interface Cards {
    [key: string]: Card
}

