import React, { useEffect } from 'react'
import { ToastContainer, toast } from '../utils/toast'

import { endpoint } from '../apis'
import socketIOClient from 'socket.io-client'

interface Props {
	callback: (point: string) => void
}

function newRecordMessage(user_id: string) {
	return user_id === localStorage.getItem('user_id') ? 'You just break the grobal highest record' : `Someone just break the grobal highest record`
}

function Notification(props: Props) {
	useEffect(() => {
		const socket = socketIOClient(endpoint)
		socket.on('boardcast', (data: any) => {
			props.callback(data)
			toast.notify(newRecordMessage(data.user_id), {
				type: 'success',
				position: 'top',
				title: 'Announcement notification'
			})
		})
	}, [])

	return (
		<div>
			<ToastContainer align={"center"} position={"top"} />
		</div>
	)
}

export default Notification