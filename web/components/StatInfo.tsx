import React from 'react'
import styles from './statInfo.module.scss'

interface Stat {
    icon: string,
    title: string,
    point: number | string
}

interface Props {
    stats: Stat[],
    onClickNewGameCallback: () => void
}

function StatInfo(props: Props) {
	return (
		<div className={`${styles['stat-info']}`}>
            {
                props.stats.map((stat) => (
                    <div className={styles['item']}>
                        <img src={stat.icon} width={48} height={48} />
                        <h3 className={styles['title']}>{stat.title}</h3>
                        <div className={styles['space']}></div>
                        <h3>{stat.point}</h3>
                    </div>
                ))
            }
            <div style={{ flex: 1 }}>
            </div>
            <button className={styles['new-game-button']} onClick={props.onClickNewGameCallback}>
                NEW GAME
            </button>
        </div>
	)
}

export default StatInfo