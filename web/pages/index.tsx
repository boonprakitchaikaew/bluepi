import './styles.scss'

import * as apis from '../apis'

import { Cards, GetNewGameResponse, GetScoresResponse, OpenCardResponse, RegisterResponse } from '../interfaces'
import React, { Component } from 'react'

import Notification from '../components/Notification'
import StatInfo from '../components/StatInfo'
import _ from 'lodash'
import styles from './index.module.scss'

export interface State {
	isLoading: boolean,
	cards: Cards,
	gameId: string,
	totalClicked: string,
	myBestScore: number | null,
	grobalBestScore: number | null,
	openedCard1: string,
	openedCard2: string
}

const defaultState: State = {
	isLoading: true,
	cards: {},
	gameId: '',
	totalClicked: '0',
	myBestScore: null,
	grobalBestScore: null,
	openedCard1: '',
	openedCard2: ''
}

class IndexPage extends Component {
	state: State = defaultState

	componentDidMount = async () => {
		this.fetchRequireData()
	}

	fetchRequireData = async () => {
		const token = localStorage.getItem('token')
		// check is already authenticated
		if(!token) {
			const registerResponse: RegisterResponse = await apis.register()
			localStorage.setItem('token', registerResponse.token)
			localStorage.setItem('user_id', registerResponse.user_id)
		}
		
		// fetch best scores
		await this.fetchScores()

		// start a game
		const getNewGameResponse: GetNewGameResponse = await apis.getNewGame()

		this.setState({
			cards: _.shuffle(getNewGameResponse.cards).reduce((o, n: string) => {
				return {
					...o,
					[n]: {
						id: n,
						number: null,
						isOpened: false
					}
				}
			}, {}),
			gameId: getNewGameResponse.id,
			isLoading: false
		})
	}

	onNewGameClicked = () => {
		this.setState({
			...defaultState,
			isLoading: false
		})
		this.fetchRequireData()
	}

	fetchScores = async () => {
		// fetch best scores
		const getScoresResponse: GetScoresResponse = await apis.getScores()
		this.setState({
			myBestScore: getScoresResponse.my_best_score,
			grobalBestScore: getScoresResponse.grobal_best_score,
		})
	}
	
	onCardClicked = async (cardId: string) => {
		const { cards, openedCard1, gameId } = this.state

		// avoiding double click 
		if(openedCard1 && this.state.openedCard2) return
		if(cards[cardId].isMatched || cards[cardId].isOpened) return 
		
		const openCardResponse: OpenCardResponse = await apis.openCard(gameId, cardId)
		
		// is first open card
		if(!openedCard1) {
			this.setState({
				cards: {
					...cards,
					[cardId]: {
						id: cardId,
						number: openCardResponse.number,
						isOpened: true
					}
				},
				openedCard1: cardId
			})
		} else {
			this.setState({
				cards: {
					...cards,
					[cardId]: {
						id: cardId,
						number: openCardResponse.number,
						isOpened: true
					}
				},
				openedCard2: cardId
			}, () => {
				const { openedCard2, cards } = this.state

				// checking is matched
				if(cards[openedCard1].number == cards[openedCard2].number) {
					this.setState({
						cards: {
							...cards,
							[openedCard1]: {
								...cards[openedCard1],
								isMatched: true
							},
							[openedCard2]: {
								...cards[openedCard2],
								isMatched: true
							}
						},
						openedCard1: '',
						openedCard2: ''
					}, async () => {
						// checking is all matched
						if(Object.values(this.state.cards).some((card) => !card.isMatched)) return
						
						await this.fetchScores()
					})
				} else {
					// clear number and flip to back
					setTimeout(() => {
						this.setState({
							cards: {
								...cards,
								[openedCard1]: {
									...cards[openedCard1],
									number: '',
									isOpened: false
								},
								[openedCard2]: {
									...cards[openedCard2],
									number: '',
									isOpened: false
								}
							},
							openedCard1: '',
							openedCard2: ''
						})
					}, 1000)
				}
			})
		}

		this.setState({
			totalClicked: openCardResponse.total_clicked,
			curruntlyOpenNumber: openCardResponse.total_clicked
		})
	}

	render() {
		const { totalClicked, myBestScore, grobalBestScore, isLoading, cards } = this.state
		
		if(isLoading) return <div />

		return (
			<div className={styles['game-page-container']}>
				<StatInfo
					stats={[
						{
							icon: 'https://cdn1.iconfinder.com/data/icons/seo-flat-shaded/512/Hand_Click-512.png',
							title: 'Clicked',
							point: totalClicked
						},
						{
							icon: 'https://icon-library.com/images/high-score-icon/high-score-icon-24.jpg',
							title: 'My Best',
							point: myBestScore || '-'
						},
						{
							icon: 'https://cdn1.iconfinder.com/data/icons/game-design-butterscotch-vol-1/256/Leader-512.png',
							title: 'Grobal Best',
							point: grobalBestScore || '-'
						}
					]}
					onClickNewGameCallback={this.onNewGameClicked}
				/>
				<div className={styles['card-container']} style={{ flex: 1 }}>
					{
						Object.values(cards).map(({ id: cardId, number, isOpened, isMatched }) => (
							<div className={styles['flip-card']} onClick={() => this.onCardClicked(cardId)}>
								<div className={`${styles['flip-card-inner']} ${isOpened || isMatched ? styles['selected'] : ''}`}>
									<div className={styles['flip-card-front']}>
									</div>
									<div className={styles['flip-card-back']}>
										<h1>{number}</h1>
									</div>
								</div>
							</div>
						))
					}
				</div>
				<Notification callback={this.fetchScores} />
			</div>
		)
	}
}

export default IndexPage
