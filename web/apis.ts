import axios from 'axios'

export const endpoint = ''
axios.defaults.baseURL = `${endpoint}/api`

export const register = () => axios.post(`/users/`, {}).then(({ data }) => data)

export const getNewGame = () => axios.post(`/games/`, {}, {
    headers: { Authorization: `${localStorage.getItem('token')}`}
}).then(({ data }) => data)

export const openCard = (gameId: string, cardId: string) => axios.post(`/games/${gameId}`, {
    card_id: cardId
}, {
    headers: { Authorization: `${localStorage.getItem('token')}`}
}).then(({ data }) => data)

export const getScores = () => axios.get(`/games/scores`, {
    headers: { Authorization: `${localStorage.getItem('token')}`}
}).then(({ data }) => data)